'use strict'

###*
 # @ngdoc overview
 # @name dotinstallAngularYoutubeApp
 # @description
 # # dotinstallAngularYoutubeApp
 #
 # Main module of the application.
###
angular
  .module('dotinstallAngularYoutubeApp', [])
